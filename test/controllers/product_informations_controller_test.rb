require 'test_helper'

class ProductInformationsControllerTest < ActionController::TestCase
  setup do
    @product_information = product_informations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_informations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_information" do
    assert_difference('ProductInformation.count') do
      post :create, product_information: { active: @product_information.active, attribute_title: @product_information.attribute_title, produt_id: @product_information.produt_id, published: @product_information.published, value: @product_information.value }
    end

    assert_redirected_to product_information_path(assigns(:product_information))
  end

  test "should show product_information" do
    get :show, id: @product_information
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_information
    assert_response :success
  end

  test "should update product_information" do
    patch :update, id: @product_information, product_information: { active: @product_information.active, attribute_title: @product_information.attribute_title, produt_id: @product_information.produt_id, published: @product_information.published, value: @product_information.value }
    assert_redirected_to product_information_path(assigns(:product_information))
  end

  test "should destroy product_information" do
    assert_difference('ProductInformation.count', -1) do
      delete :destroy, id: @product_information
    end

    assert_redirected_to product_informations_path
  end
end
