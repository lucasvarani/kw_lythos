require 'test_helper'

class HighlightsControllerTest < ActionController::TestCase
  setup do
    @highlight = highlights(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:highlights)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create highlight" do
    assert_difference('Highlight.count') do
      post :create, highlight: { active: @highlight.active, color1: @highlight.color1, color2: @highlight.color2, color3: @highlight.color3, color4: @highlight.color4, image_highlight: @highlight.image_highlight, link: @highlight.link, order: @highlight.order, publication_start: @highlight.publication_start, publication_stop: @highlight.publication_stop, published: @highlight.published, subtitle: @highlight.subtitle, thumbnail: @highlight.thumbnail, thumbnail_shadow: @highlight.thumbnail_shadow, title: @highlight.title }
    end

    assert_redirected_to highlight_path(assigns(:highlight))
  end

  test "should show highlight" do
    get :show, id: @highlight
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @highlight
    assert_response :success
  end

  test "should update highlight" do
    patch :update, id: @highlight, highlight: { active: @highlight.active, color1: @highlight.color1, color2: @highlight.color2, color3: @highlight.color3, color4: @highlight.color4, image_highlight: @highlight.image_highlight, link: @highlight.link, order: @highlight.order, publication_start: @highlight.publication_start, publication_stop: @highlight.publication_stop, published: @highlight.published, subtitle: @highlight.subtitle, thumbnail: @highlight.thumbnail, thumbnail_shadow: @highlight.thumbnail_shadow, title: @highlight.title }
    assert_redirected_to highlight_path(assigns(:highlight))
  end

  test "should destroy highlight" do
    assert_difference('Highlight.count', -1) do
      delete :destroy, id: @highlight
    end

    assert_redirected_to highlights_path
  end
end
