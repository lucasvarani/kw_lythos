require 'test_helper'

class SystemConfigurationsControllerTest < ActionController::TestCase
  setup do
    @system_configuration = system_configurations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:system_configurations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create system_configuration" do
    assert_difference('SystemConfiguration.count') do
      post :create, system_configuration: { commercial_catalog_content_type: @system_configuration.commercial_catalog_content_type, commercial_catalog_file_name: @system_configuration.commercial_catalog_file_name, commercial_catalog_file_size: @system_configuration.commercial_catalog_file_size, commercial_catalog_updated_at: @system_configuration.commercial_catalog_updated_at, facebook_link: @system_configuration.facebook_link, institutional_image_content_type: @system_configuration.institutional_image_content_type, institutional_image_file_name: @system_configuration.institutional_image_file_name, institutional_image_file_size: @system_configuration.institutional_image_file_size, institutional_image_updated_at: @system_configuration.institutional_image_updated_at, institutional_text: @system_configuration.institutional_text, institutional_title: @system_configuration.institutional_title, link_instagram: @system_configuration.link_instagram, link_pinterest: @system_configuration.link_pinterest, restricted_area_link: @system_configuration.restricted_area_link, senders_contact: @system_configuration.senders_contact, senders_where_to_find: @system_configuration.senders_where_to_find, technical_guide_content_type: @system_configuration.technical_guide_content_type, technical_guide_file_name: @system_configuration.technical_guide_file_name, technical_guide_file_size: @system_configuration.technical_guide_file_size, technical_guide_updated_at: @system_configuration.technical_guide_updated_at }
    end

    assert_redirected_to system_configuration_path(assigns(:system_configuration))
  end

  test "should show system_configuration" do
    get :show, id: @system_configuration
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @system_configuration
    assert_response :success
  end

  test "should update system_configuration" do
    patch :update, id: @system_configuration, system_configuration: { commercial_catalog_content_type: @system_configuration.commercial_catalog_content_type, commercial_catalog_file_name: @system_configuration.commercial_catalog_file_name, commercial_catalog_file_size: @system_configuration.commercial_catalog_file_size, commercial_catalog_updated_at: @system_configuration.commercial_catalog_updated_at, facebook_link: @system_configuration.facebook_link, institutional_image_content_type: @system_configuration.institutional_image_content_type, institutional_image_file_name: @system_configuration.institutional_image_file_name, institutional_image_file_size: @system_configuration.institutional_image_file_size, institutional_image_updated_at: @system_configuration.institutional_image_updated_at, institutional_text: @system_configuration.institutional_text, institutional_title: @system_configuration.institutional_title, link_instagram: @system_configuration.link_instagram, link_pinterest: @system_configuration.link_pinterest, restricted_area_link: @system_configuration.restricted_area_link, senders_contact: @system_configuration.senders_contact, senders_where_to_find: @system_configuration.senders_where_to_find, technical_guide_content_type: @system_configuration.technical_guide_content_type, technical_guide_file_name: @system_configuration.technical_guide_file_name, technical_guide_file_size: @system_configuration.technical_guide_file_size, technical_guide_updated_at: @system_configuration.technical_guide_updated_at }
    assert_redirected_to system_configuration_path(assigns(:system_configuration))
  end

  test "should destroy system_configuration" do
    assert_difference('SystemConfiguration.count', -1) do
      delete :destroy, id: @system_configuration
    end

    assert_redirected_to system_configurations_path
  end
end
