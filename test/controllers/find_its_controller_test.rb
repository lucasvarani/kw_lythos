require 'test_helper'

class FindItsControllerTest < ActionController::TestCase
  setup do
    @find_it = find_its(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:find_its)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create find_it" do
    assert_difference('FindIt.count') do
      post :create, find_it: { active: @find_it.active, city: @find_it.city, email: @find_it.email, name: @find_it.name, phone: @find_it.phone, published: @find_it.published, state: @find_it.state }
    end

    assert_redirected_to find_it_path(assigns(:find_it))
  end

  test "should show find_it" do
    get :show, id: @find_it
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @find_it
    assert_response :success
  end

  test "should update find_it" do
    patch :update, id: @find_it, find_it: { active: @find_it.active, city: @find_it.city, email: @find_it.email, name: @find_it.name, phone: @find_it.phone, published: @find_it.published, state: @find_it.state }
    assert_redirected_to find_it_path(assigns(:find_it))
  end

  test "should destroy find_it" do
    assert_difference('FindIt.count', -1) do
      delete :destroy, id: @find_it
    end

    assert_redirected_to find_its_path
  end
end
