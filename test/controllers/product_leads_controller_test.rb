require 'test_helper'

class ProductLeadsControllerTest < ActionController::TestCase
  setup do
    @product_lead = product_leads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_leads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_lead" do
    assert_difference('ProductLead.count') do
      post :create, product_lead: { email: @product_lead.email, name: @product_lead.name, phone: @product_lead.phone, product: @product_lead.product }
    end

    assert_redirected_to product_lead_path(assigns(:product_lead))
  end

  test "should show product_lead" do
    get :show, id: @product_lead
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_lead
    assert_response :success
  end

  test "should update product_lead" do
    patch :update, id: @product_lead, product_lead: { email: @product_lead.email, name: @product_lead.name, phone: @product_lead.phone, product: @product_lead.product }
    assert_redirected_to product_lead_path(assigns(:product_lead))
  end

  test "should destroy product_lead" do
    assert_difference('ProductLead.count', -1) do
      delete :destroy, id: @product_lead
    end

    assert_redirected_to product_leads_path
  end
end
