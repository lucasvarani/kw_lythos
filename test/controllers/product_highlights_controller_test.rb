require 'test_helper'

class ProductHighlightsControllerTest < ActionController::TestCase
  setup do
    @product_highlight = product_highlights(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_highlights)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_highlight" do
    assert_difference('ProductHighlight.count') do
      post :create, product_highlight: { active: @product_highlight.active, color1: @product_highlight.color1, color2: @product_highlight.color2, color3: @product_highlight.color3, color4: @product_highlight.color4, highlight_product: @product_highlight.highlight_product, link: @product_highlight.link, order: @product_highlight.order, product_id: @product_highlight.product_id, publication_start: @product_highlight.publication_start, publication_stop: @product_highlight.publication_stop, published: @product_highlight.published, short_text: @product_highlight.short_text, subtitle: @product_highlight.subtitle, title: @product_highlight.title }
    end

    assert_redirected_to product_highlight_path(assigns(:product_highlight))
  end

  test "should show product_highlight" do
    get :show, id: @product_highlight
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_highlight
    assert_response :success
  end

  test "should update product_highlight" do
    patch :update, id: @product_highlight, product_highlight: { active: @product_highlight.active, color1: @product_highlight.color1, color2: @product_highlight.color2, color3: @product_highlight.color3, color4: @product_highlight.color4, highlight_product: @product_highlight.highlight_product, link: @product_highlight.link, order: @product_highlight.order, product_id: @product_highlight.product_id, publication_start: @product_highlight.publication_start, publication_stop: @product_highlight.publication_stop, published: @product_highlight.published, short_text: @product_highlight.short_text, subtitle: @product_highlight.subtitle, title: @product_highlight.title }
    assert_redirected_to product_highlight_path(assigns(:product_highlight))
  end

  test "should destroy product_highlight" do
    assert_difference('ProductHighlight.count', -1) do
      delete :destroy, id: @product_highlight
    end

    assert_redirected_to product_highlights_path
  end
end
