class Notifier < ActionMailer::Base
  helper :application
  default :from => "Lythos Teste Site Novo<no-reply@sato7.com.br>"

  def send_contact_user(contact)
    @contact = contact
    mail(:to => @contact.email, :bcc => "log@korewa.com.br", :subject => "Contato enviado - Protocolo Nº#{@contact.id} ")
  end

  def send_contact(contact)
    @system_configuration = SystemConfiguration.find(1)
    @contact = contact
    mail(:to => @system_configuration.senders_contact, :bcc => "log@korewa.com.br", :subject => "Novo contato pelo site - Protocolo Nº#{@contact.id} ")
  end

  def send_budget_user(budget)
    @budget = budget
    mail(:to => @budget.email, :bcc => "log@korewa.com.br", :subject => "Orçamento enviado - Protocolo Nº#{@budget.id} ")
  end

  def send_budget(budget)
    @system_configuration = SystemConfiguration.find(1)
    @budget = budget
    mail(:to => @system_configuration.senders_contact, :bcc => "log@korewa.com.br", :subject => "Novo orçamento pelo site - Protocolo Nº#{@budget.id} ")
  end


  def send_contact_lead_user(contact)
    @contact = contact
    mail(:to => @contact.email, :bcc => "log@korewa.com.br", :subject => "Contato enviado - Protocolo Nº#{@contact.id} ")
  end

  def send_contact_lead(contact)
    @system_configuration = SystemConfiguration.find(1)
    @contact = contact
    mail(:to => @system_configuration.senders_contact, :bcc => "log@korewa.com.br", :subject => "Novo contato pelo site - Protocolo Nº#{@contact.id} ")
  end

  def send_product_lead(product_lead)
    @system_configuration = SystemConfiguration.find(1)
    @product_lead = product_lead
    mail(:to => @system_configuration.senders_contact, :bcc => "log@korewa.com.br", :subject => "Novo lead de produto pelo site - Protocolo Nº#{@product_lead.id} ")
  end

  def send_order_user(order)
    @order = order
    mail(:to => @order.email, :bcc => "log@korewa.com.br", :subject => "Orçamento enviado - Protocolo Nº#{@order.id} ")
  end

  def send_order(order)
    @order = order
    @order_products = @order.order_products.all
    mail(:to => "info@lythos.com.br ", :bcc => "log@korewa.com.br", :subject => "Novo orçamento solicitado site - Protocolo Nº#{@order.id} ")
  end

  def send_findIt_user(contact)
    @contact = contact
    mail(:to => @contact.email, :bcc => "log@korewa.com.br", :subject => "Contato enviado - Protocolo Nº#{@contact.id} ")
  end

  def send_findIt(contact)
    @system_configuration = SystemConfiguration.find(1)
    @contact = contact
    mail(:to => @system_configuration.senders_where_to_find, :bcc => "log@korewa.com.br", :subject => "Novo contato pelo site - Protocolo Nº#{@contact.id} ")
  end
end
