class HomesController < ApplicationController
  before_filter :load_application
  def index
    @highlights = Highlight.where("active=1 and published=1 and publication_stop >= ? and publication_start <= ?", DateTime.now, DateTime.now).order("`order`, `updated_at`") #.order("`order`, `updated_at`")
    if @highlights.count > 0
      @defaultcolors = [@highlights.first.color1, @highlights.first.color2, @highlights.first.color3, @highlights.first.color4]
    end
  end

end
