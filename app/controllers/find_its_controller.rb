class FindItsController < ApplicationController
  before_filter :load_application

  def new
    @find_it = FindIt.new
  end

  def create
    @find_it = FindIt.new(find_it_params)
    if @find_it.save
      Notifier.send_findIt(@find_it).deliver
      Notifier.send_findIt_user(@find_it).deliver
    else
      @erro = true
    end

  end

  def show
  end

  private
    def find_it_params
      params.require(:find_it).permit!
    end
end
