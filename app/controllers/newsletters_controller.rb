class NewslettersController < ApplicationController

  def create
    @newsletter = Newsletter.new(newsletter_params)
    if !@newsletter.save
      @erro = true
    end
  end

  private
    def newsletter_params
      params.require(:newsletter).permit!
    end

end
