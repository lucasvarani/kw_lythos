class ProductLeadsController < ApplicationController
  # before_filter :load_application

  def create
    @product_lead = ProductLead.new(product_lead_params)
    respond_to do |f|
      if @product_lead.save
        Notifier.send_product_lead(@product_lead).deliver
        f.js { render layout: false, content_type: 'text/javascript' }
        f.html
      else
        @erro = true
      end
    end
  end

  private
    def product_lead_params
      params.require(:product_lead).permit!
    end
end
