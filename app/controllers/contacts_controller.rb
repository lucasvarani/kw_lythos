class ContactsController < ApplicationController
  before_filter :load_application
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      Notifier.send_contact(@contact).deliver
      Notifier.send_contact_user(@contact).deliver
    else
      @erro = true
    end
  end

  def show

  end

  private
    def contact_params
      params.require(:contact).permit!
    end
end
