class Admin::ContactLeadsController < ApplicationController
  before_action :set_contact, only: [:show]
  layout "admin"

  # GET /admin/contacts
  # GET /admin/contacts.json
  def index
    @contacts = ContactLead.all.order("id DESC")
  end

  # GET /admin/contacts/1
  # GET /admin/contacts/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = ContactLead.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit!
    end
end
