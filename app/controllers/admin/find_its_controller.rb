class Admin::FindItsController < ApplicationController
  before_action :set_find_it, only: [:show, :edit, :update, :destroy]
  layout "admin"

  # GET /find_its
  # GET /find_its.json
  def index
    @find_its = FindIt.all
  end

  # GET /find_its/1
  # GET /find_its/1.json
  def show
  end

  # GET /find_its/new
  def new
    @find_it = FindIt.new
  end

  # GET /find_its/1/edit
  def edit
  end

  # POST /find_its
  # POST /find_its.json
  def create
    @find_it = FindIt.new(find_it_params)

    respond_to do |format|
      if @find_it.save
        format.html { redirect_to @find_it, notice: 'Find it was successfully created.' }
        format.json { render :show, status: :created, location: @find_it }
      else
        format.html { render :new }
        format.json { render json: @find_it.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /find_its/1
  # PATCH/PUT /find_its/1.json
  def update
    respond_to do |format|
      if @find_it.update(find_it_params)
        format.html { redirect_to @find_it, notice: 'Find it was successfully updated.' }
        format.json { render :show, status: :ok, location: @find_it }
      else
        format.html { render :edit }
        format.json { render json: @find_it.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /find_its/1
  # DELETE /find_its/1.json
  def destroy
    @find_it.destroy
    respond_to do |format|
      format.html { redirect_to find_its_url, notice: 'Find it was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_find_it
      @find_it = FindIt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def find_it_params
      params.require(:find_it).permit(:name, :email, :phone, :city, :state, :published, :active)
    end
end
