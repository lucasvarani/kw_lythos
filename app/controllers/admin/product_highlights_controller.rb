class Admin::ProductHighlightsController < ApplicationController
  before_action :set_product_highlight, only: [:show, :edit, :update, :destroy]
  before_filter :load_product

  layout "admin"

  # GET /product_highlights
  # GET /product_highlights.json
  def index
    @product_highlights = @product.product_highlights.all
  end

  # GET /product_highlights/1
  # GET /product_highlights/1.json
  def show
    @product_highlight = @product.product_highlights.find(params[:id])
  end

  # GET /product_highlights/new
  def new
    @product_highlight = @product.product_highlights.build
  end

  # GET /product_highlights/1/edit
  def edit
    @product_highlight = @product.product_highlights.find(params[:id])
  end

  # POST /product_highlights
  # POST /product_highlights.json
  def create
    @product_highlight = @product.product_highlights.build(product_highlight_params)

    respond_to do |format|
      if @product_highlight.save
        format.html { redirect_to [:admin, @product, @product_highlight], notice: 'Product highlight was successfully created.' }
        format.json { render :show, status: :created, location: @product_highlight }
      else
        format.html { render :new }
        format.json { render json: [:admin, @product, @product_highlight.errors], status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_highlights/1
  # PATCH/PUT /product_highlights/1.json
  def update
    respond_to do |format|
      if @product_highlight.update(product_highlight_params)
        format.html { redirect_to [:admin, @product, @product_highlight], notice: 'Product highlight was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_highlight }
      else
        format.html { render :edit }
        format.json { render json: [:admin, @product, @product_highlight.errors], status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_highlights/1
  # DELETE /product_highlights/1.json
  def destroy
    @product_highlight.destroy
    respond_to do |format|
      format.html { redirect_to admin_product_product_highlights_url, notice: 'Product highlight was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def load_product
      @product = Product.find(params[:product_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_product_highlight
      @product_highlight = ProductHighlight.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_highlight_params
      params.require(:product_highlight).permit!
    end
end
