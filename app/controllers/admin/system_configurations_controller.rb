class Admin::SystemConfigurationsController < ApplicationController
  # before_action :set_system_configuration, only: [:show, :edit, :update, :destroy]
  layout "admin"
  # GET /system_configurations
  # GET /system_configurations.json
  def index
    redirect_to admin_system_configuration_path(1)
  end

  # GET /system_configurations/1
  # GET /system_configurations/1.json
  def show
    @system_configuration = SystemConfiguration.find(1)
  end

  # GET /system_configurations/new
  def new
    redirect_to admin_system_configuration_path(1)
  end

  # GET /system_configurations/1/edit
  def edit
    @system_configuration = SystemConfiguration.find(1)
  end

  # POST /system_configurations
  # POST /system_configurations.json
  def create
    redirect_to admin_system_configuration_path(1)
  end

  # PATCH/PUT /system_configurations/1
  # PATCH/PUT /system_configurations/1.json
  def update
    @system_configuration = SystemConfiguration.find(1)
    respond_to do |format|
      if @system_configuration.update(system_configuration_params)
        format.html { redirect_to [:admin, @system_configuration], notice: 'System configuration was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @system_configuration] }
      else
        format.html { render :edit }
        format.json { render json: @system_configuration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /system_configurations/1
  # DELETE /system_configurations/1.json
  def destroy
    redirect_to admin_system_configuration_path(1)
  end

  def not_commercial_catalog
    @system_configuration = SystemConfiguration.find(1)
    @system_configuration.commercial_catalog = nil
    @system_configuration.save
    redirect_to admin_system_configuration_path(1)
  end

  def not_technical_guide
    @system_configuration = SystemConfiguration.find(1)
    @system_configuration.technical_guide = nil
    @system_configuration.save
    redirect_to admin_system_configuration_path(1)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_system_configuration
      @system_configuration = SystemConfiguration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def system_configuration_params
      params.require(:system_configuration).permit!
    end
end
