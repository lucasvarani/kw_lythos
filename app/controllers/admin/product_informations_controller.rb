class Admin::ProductInformationsController < ApplicationController
  before_action :set_product_information, only: [:show, :edit, :update, :destroy]
  before_action :load_product

  layout "admin"
  # GET /product_informations
  # GET /product_informations.json
  def index
    @product_informations = ProductInformation.all
  end

  # GET /product_informations/1
  # GET /product_informations/1.json
  def show
  end

  # GET /product_informations/new
  def new
    @product_information = ProductInformation.new
  end

  # GET /product_informations/1/edit
  def edit
  end

  # POST /product_informations
  # POST /product_informations.json
  def create
    @product_information = ProductInformation.new(product_information_params)

    respond_to do |format|
      if @product_information.save
        format.html { redirect_to [:admin, @product, @product_information], notice: 'Product information was successfully created.' }
        format.json { render :show, status: :created, location: @product_information }
      else
        format.html { render :new }
        format.json { render json: [:admin, @product, @product_information.errors], status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_informations/1
  # PATCH/PUT /product_informations/1.json
  def update
    respond_to do |format|
      if @product_information.update(product_information_params)
        format.html { redirect_to [:admin, @product, @product_information], notice: 'Product information was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_information }
      else
        format.html { render :edit }
        format.json { render json: [:admin, @product, @product_information.errors], status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_informations/1
  # DELETE /product_informations/1.json
  def destroy
    @product_information.destroy
    respond_to do |format|
      format.html { redirect_to admin_product_product_informations_url, notice: 'Product information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def load_product
      @product = Product.find(params[:product_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_product_information
      @product_information = ProductInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_information_params
      params.require(:product_information).permit(:attribute_title, :value, :active, :published, :produt_id)
    end
end
