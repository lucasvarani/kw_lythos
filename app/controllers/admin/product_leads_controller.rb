class Admin::ProductLeadsController < ApplicationController
  before_action :set_contact, only: [:show]
  layout "admin"

  # GET /admin/contacts
  # GET /admin/contacts.json
  def index
    @product_leads = ProductLead.all.order("id DESC")
  end

  # GET /admin/contacts/1
  # GET /admin/contacts/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @product_lead = ProductLead.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_lead_params
      params.require(:product_lead).permit!
    end
end
