class ProductsController < ApplicationController
  before_filter :load_application
  def index
    @products_highlights = ProductHighlight.where("published = true and active = true and product_id in (SELECT id FROM `products` where published = 1 and active = 1)")
    if @products_highlights.count > 0
      @defaultcolors = [@products_highlights.first.color1, @products_highlights.first.color2, @products_highlights.first.color3, @products_highlights.first.color4]
    end
  end

  def show
    @product = Product.where('published =  true and active = true').find(params[:id])
    if @product.blank?
      redirect_to root_path
    end
    @colors = @product.colors
    # @informations = @product.product_informations.all_published
    # @product_highlights = @product.product_highlights.all_published
    # if @product_highlights.count > 0
    #   @defaultcolors = [@product_highlights.first.color1, @product_highlights.first.color2, @product_highlights.first.color3, @product_highlights.first.color4]
    # end
  end

end
