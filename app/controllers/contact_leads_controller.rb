class ContactLeadsController < ApplicationController
  # before_filter :load_application

  def create
    @contact_lead = ContactLead.new(contact_lead_params)
    respond_to do |f|
      if @contact_lead.save
        Notifier.send_contact_lead(@contact_lead).deliver
        Notifier.send_contact_lead_user(@contact_lead).deliver
        f.js { render layout: false, content_type: 'text/javascript' }
        f.html
      else
        @erro = true
      end
    end
  end

  private
    def contact_lead_params
      params.require(:contact_lead).permit!
    end
end
