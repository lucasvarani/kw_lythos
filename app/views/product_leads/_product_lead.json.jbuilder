json.extract! product_lead, :id, :name, :email, :phone, :product, :created_at, :updated_at
json.url product_lead_url(product_lead, format: :json)
