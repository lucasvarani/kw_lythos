json.extract! budget, :id, :name, :email, :phone, :product_id, :qntd, :message, :created_at, :updated_at
json.url budget_url(budget, format: :json)
