json.extract! contact, :id, :name, :email, :phone, :message, :published, :state, :business, :city, :active, :created_at, :updated_at
json.url contact_url(contact, format: :json)
