json.extract! find_it, :id, :name, :email, :phone, :city, :state, :published, :active, :created_at, :updated_at
json.url find_it_url(find_it, format: :json)
