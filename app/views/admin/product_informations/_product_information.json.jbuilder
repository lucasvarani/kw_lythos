json.extract! product_information, :id, :attribute_title, :value, :active, :published, :produt_id, :created_at, :updated_at
json.url product_information_url(product_information, format: :json)
