json.extract! product_highlight, :id, :product_id, :title, :subtitle, :publication_start, :publication_stop, :order, :active, :published, :link, :color1, :color2, :color3, :color4, :highlight_product, :short_text, :created_at, :updated_at
json.url product_highlight_url(product_highlight, format: :json)
