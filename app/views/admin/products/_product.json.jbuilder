json.extract! product, :id, :title, :description, :active, :published, :brand, :style, :code, :product_image, :display_picture, :image_entry, :reference, :created_at, :updated_at
json.url product_url(product, format: :json)
