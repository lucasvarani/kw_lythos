json.extract! highlight, :id, :title, :subtitle, :publication_start, :publication_stop, :order, :published, :active, :image_highlight, :thumbnail, :thumbnail_shadow, :link, :color1, :color2, :color3, :color4, :created_at, :updated_at
json.url highlight_url(highlight, format: :json)
