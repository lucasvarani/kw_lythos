json.extract! product, :id, :title, :active, :published, :image_product, :created_at, :updated_at
json.url product_url(product, format: :json)
