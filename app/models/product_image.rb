class ProductImage < ActiveRecord::Base
  belongs_to :product
  has_attached_file :image_product, styles: {
                      :medium => "350x400#",
                      :thumb => "93x93#"
                    }
  validates_attachment_content_type :image_product, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def url_slug()
    "#{product_id}-#{product.title.parameterize}"
  end
end
