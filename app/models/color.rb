class Color < ActiveRecord::Base
  has_and_belongs_to_many :products, dependent: :destroy

  def color_name
    "#{color} - #{name}"
  end
end
