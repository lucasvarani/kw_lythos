class Product < ActiveRecord::Base

  has_many :product_informations
  has_many :product_highlights
  has_many :product_images
  has_and_belongs_to_many :colors

  has_attached_file :product_image,:styles => {:small => "100x100#", :large => "500x500>"}
  has_attached_file :display_picture,:styles => {:small => "100x100#", :large => "500x500>"}
  has_attached_file :image_entry,:styles => {:small => "100x100#", :large => "200x400#"}
  has_attached_file :sketchup

  validates_attachment_content_type :product_image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_content_type :display_picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_content_type :image_entry, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_file_name :sketchup, matches: [/skp\z/]

  def url_slug()
    "#{id}-#{title.parameterize}"
  end

end
