class SystemConfiguration < ActiveRecord::Base

  has_attached_file :commercial_catalog
  has_attached_file :technical_guide

  has_attached_file :institutional_image,:styles => {:small => "100x100#", :home => "348x400>"}

  validates_attachment_content_type :institutional_image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates_attachment_content_type :commercial_catalog, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", 'application/pdf', 'application/msword', 'text/plain']

  validates_attachment_content_type :technical_guide, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", 'application/pdf', 'application/msword', 'text/plain']

end
