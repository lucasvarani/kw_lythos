if ($(window).width() > 768){
  $('#destaque').on('cycle-update-view', function(event, optionHash) {
    $('header').height($("#destaque").height());
  });
  // $('#destaque').cycle({
  //   fx: "fadeout",
  //   pauseOnHover: true,
  //   slides: '> div',
  //   timeout: 8000,
  // });
	$('#destaque').on( 'cycle-after', function(e, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
	    var a = $('.cycle-slide-active').find('img');
			$(".base-color1").css('backgroundColor','#' + a.attr("data-color1"));
	    $(".base-color2").css('backgroundColor','#' + a.attr("data-color2"));
	    $(".base-color3").css('backgroundColor','#' + a.attr("data-color3"));
	    $(".base-color4").css('backgroundColor','#' + a.attr("data-color4"));

	});

	$(window).resize(function(){
    $('header').height($("#destaque").height());
	});
}

// if ($(window).width() > 968){
//   $('#destaque-interna').on('cycle-update-view', function(event, optionHash) {
//     $('header').height($("#destaque-interna").height());
//   });
//   $('#destaque-interna').cycle({
//     fx: "fadeout",
//     pauseOnHover: true,
//     speed: 100,
//     slides: '> div',
//     timeout: 0,
// 	next:   '#next2',
//     prev:   '#prev2'
//   });
// 	$(window).resize(function(){
//     $('header').height($("#destaque-interna").height());
// 	});
// }


 $(document).ready(function() {
	$(".drop-menu").hover(function(event){
		$('.sub-menu').removeClass('hide');
	  });

	$('.sub-menu'+'.drop-menu').mouseleave(function(event){
		$('.sub-menu').addClass('hide');
	  });

	$('.drop-menu').mouseleave(function(event){
		$('.sub-menu').addClass('hide');
	  });


});

 if ($(".single-image-slider").length) {
  $(document).ready(function(){
    $(".tosrus a").tosrus();
    $('.single-image-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.single-image-slider',
      focusOnSelect: true
    });
  });
 }