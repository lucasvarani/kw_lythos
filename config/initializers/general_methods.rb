class ActiveRecord::Base
  #Metodo que substitui o destroy
  def newdestroy
    self.active = false
    self.save
  end

  private
  # Metodo para listar somente os ativos (Não "Deletados")
  def self.all_active
    where("active = 1").order('id')
  end

  # Metodo para listar todos publicados
  def self.all_published
    where("published = 1 and active = 1").order('id')
  end

  def self.find_published(id)
    where('published =  true and active = true').find(id)
  end

end