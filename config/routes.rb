Rails.application.routes.draw do

  resources :product_leads
  resources :newsletters, :only => [:show, :create]
  # post '/tinymce_assets' => 'tinymce_assets#create'
  resources :products, :only => [:index, :show]
  resources :contacts, :only => [:new, :show, :create]
  resources :contact_leads
  resources :find_its, :only => [:new, :show, :create]
  resources :homes, :only => [:index]

  # ADMIN
  namespace :admin do
    resources :product_leads
    resources :budgets
    resources :contact_leads
    resources :colors
    resources :users
    resources :contacts
    resources :products do
      resources :product_informations
      resources :product_highlights
      resources :product_images
    end
    resources :newsletters
    resources :highlights
    resources :find_its
    resources :system_configurations do
      member do
        get :not_commercial_catalog
        get :not_technical_guide
      end
    end
    root :to => 'statics#index'
  end
  #/ADMIN

  resource :user_session, only: [:create, :new, :destroy]

  root to: 'homes#index'

  match 'register', to: "users#new", via: :all
  match 'login', to: 'user_sessions#new', via: :all
  match 'logout', to: 'user_sessions#destroy', via: :all

end