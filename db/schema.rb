# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171020190039) do

  create_table "budgets", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.integer  "product_id", limit: 4
    t.string   "qntd",       limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "colors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "color",      limit: 255
    t.boolean  "published",              default: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "colors_products", force: :cascade do |t|
    t.integer  "product_id", limit: 4
    t.integer  "color_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "contact_leads", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.string   "cellphone",  limit: 45
    t.string   "message",    limit: 255
    t.boolean  "published",              default: true
    t.string   "state",      limit: 255
    t.string   "business",   limit: 255
    t.string   "city",       limit: 255
    t.boolean  "active",                 default: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "find_its", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.string   "city",       limit: 255
    t.string   "state",      limit: 255
    t.boolean  "published",              default: true, null: false
    t.boolean  "active",                 default: true, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "highlights", force: :cascade do |t|
    t.string   "title",                        limit: 255
    t.string   "subtitle",                     limit: 255
    t.datetime "publication_start"
    t.datetime "publication_stop"
    t.integer  "order",                        limit: 4,   default: 0,        null: false
    t.boolean  "published",                                default: true,     null: false
    t.boolean  "active",                                   default: true,     null: false
    t.string   "thumbnail",                    limit: 255
    t.string   "thumbnail_shadow",             limit: 255
    t.string   "link",                         limit: 255
    t.string   "color1",                       limit: 255, default: "5c0001"
    t.string   "color2",                       limit: 255, default: "8c0708"
    t.string   "color3",                       limit: 255, default: "c00708"
    t.string   "color4",                       limit: 255, default: "f75052"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "image_highlight_file_name",    limit: 255
    t.string   "image_highlight_content_type", limit: 255
    t.integer  "image_highlight_file_size",    limit: 4
    t.datetime "image_highlight_updated_at"
  end

  create_table "newsletters", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "product_highlights", force: :cascade do |t|
    t.integer  "product_id",                     limit: 4
    t.string   "title",                          limit: 255
    t.string   "subtitle",                       limit: 255
    t.datetime "publication_start"
    t.datetime "publication_stop"
    t.integer  "order",                          limit: 4,   default: 0,    null: false
    t.boolean  "active",                                     default: true, null: false
    t.boolean  "published",                                  default: true, null: false
    t.string   "link",                           limit: 255
    t.string   "color1",                         limit: 255
    t.string   "color2",                         limit: 255
    t.string   "color3",                         limit: 255
    t.string   "color4",                         limit: 255
    t.string   "highlight_product",              limit: 255
    t.string   "short_text",                     limit: 255
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "highlight_product_file_name",    limit: 255
    t.string   "highlight_product_content_type", limit: 255
    t.integer  "highlight_product_file_size",    limit: 4
    t.datetime "highlight_product_updated_at"
  end

  create_table "product_images", force: :cascade do |t|
    t.string   "title",                      limit: 255
    t.integer  "order",                      limit: 4,   default: 0,    null: false
    t.boolean  "active",                                 default: true, null: false
    t.boolean  "published",                              default: true, null: false
    t.string   "image_product",              limit: 255
    t.integer  "product_id",                 limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "image_product_file_name",    limit: 255
    t.string   "image_product_content_type", limit: 255
    t.integer  "image_product_file_size",    limit: 4
    t.datetime "image_product_updated_at"
  end

  create_table "product_informations", force: :cascade do |t|
    t.string   "attribute_title", limit: 255
    t.string   "value",           limit: 255
    t.boolean  "active",                      default: true, null: false
    t.boolean  "published",                   default: true, null: false
    t.integer  "product_id",      limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "product_leads", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.string   "product",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "title",                        limit: 255
    t.text     "description",                  limit: 65535
    t.boolean  "active",                                     default: true, null: false
    t.boolean  "published",                                  default: true, null: false
    t.string   "brand",                        limit: 255
    t.string   "style",                        limit: 255
    t.string   "code",                         limit: 255
    t.string   "product_image",                limit: 255
    t.string   "display_picture",              limit: 255
    t.string   "image_entry",                  limit: 255
    t.string   "reference",                    limit: 255
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "product_image_file_name",      limit: 255
    t.string   "product_image_content_type",   limit: 255
    t.integer  "product_image_file_size",      limit: 4
    t.datetime "product_image_updated_at"
    t.string   "display_picture_file_name",    limit: 255
    t.string   "display_picture_content_type", limit: 255
    t.integer  "display_picture_file_size",    limit: 4
    t.datetime "display_picture_updated_at"
    t.string   "image_entry_file_name",        limit: 255
    t.string   "image_entry_content_type",     limit: 255
    t.integer  "image_entry_file_size",        limit: 4
    t.datetime "image_entry_updated_at"
    t.string   "specifications",               limit: 255
    t.string   "sketchup_file_name",           limit: 255
    t.string   "sketchup_content_type",        limit: 255
    t.integer  "sketchup_file_size",           limit: 4
    t.datetime "sketchup_updated_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",              limit: 40
    t.string   "authorizable_type", limit: 40
    t.integer  "authorizable_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "system_configurations", force: :cascade do |t|
    t.string   "restricted_area_link",             limit: 255
    t.string   "facebook_link",                    limit: 255
    t.string   "link_instagram",                   limit: 255
    t.string   "link_pinterest",                   limit: 255
    t.string   "institutional_title",              limit: 255
    t.text     "institutional_text",               limit: 4294967295
    t.string   "senders_contact",                  limit: 255
    t.string   "senders_where_to_find",            limit: 255
    t.string   "commercial_catalog_file_name",     limit: 255
    t.string   "commercial_catalog_content_type",  limit: 255
    t.string   "commercial_catalog_file_size",     limit: 255
    t.string   "commercial_catalog_updated_at",    limit: 255
    t.string   "technical_guide_file_name",        limit: 255
    t.string   "technical_guide_content_type",     limit: 255
    t.string   "technical_guide_file_size",        limit: 255
    t.string   "technical_guide_updated_at",       limit: 255
    t.string   "institutional_image_file_name",    limit: 255
    t.string   "institutional_image_content_type", limit: 255
    t.string   "institutional_image_file_size",    limit: 255
    t.string   "institutional_image_updated_at",   limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "login",              limit: 255
    t.string   "crypted_password",   limit: 255,             null: false
    t.string   "password_salt",      limit: 255,             null: false
    t.string   "persistence_token",  limit: 255,             null: false
    t.string   "perishable_token",   limit: 255,             null: false
    t.integer  "login_count",        limit: 4,   default: 0, null: false
    t.integer  "failed_login_count", limit: 4,   default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip",   limit: 255
    t.string   "last_login_ip",      limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

end
