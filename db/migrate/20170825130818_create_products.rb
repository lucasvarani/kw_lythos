class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.text :description
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.string :brand
      t.string :style
      t.string :code
      t.string :product_image
      t.string :display_picture
      t.string :image_entry
      t.string :reference

      t.timestamps null: false
    end
  end
end
