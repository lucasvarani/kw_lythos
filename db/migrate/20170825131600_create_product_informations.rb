class CreateProductInformations < ActiveRecord::Migration
  def change
    create_table :product_informations do |t|
      t.string :attribute_title
      t.string :value
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.integer :product_id

      t.timestamps null: false
    end
  end
end
