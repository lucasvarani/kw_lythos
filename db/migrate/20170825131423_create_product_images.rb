class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images do |t|
      t.string :title
      t.integer :order,:null => false, :default => 0
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.string :image_product
      t.integer :product_id

      t.timestamps null: false
    end
  end
end
