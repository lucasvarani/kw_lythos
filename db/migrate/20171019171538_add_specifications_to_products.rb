class AddSpecificationsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :specifications, :text
  end
end
