class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors do |t|
      t.string :name
      t.string :color
      t.boolean :published, :default => 1

      t.timestamps null: false
    end
  end
end
