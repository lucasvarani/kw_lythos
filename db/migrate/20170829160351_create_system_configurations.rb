class CreateSystemConfigurations < ActiveRecord::Migration
  def change
    create_table :system_configurations do |t|
      t.string :restricted_area_link
      t.string :facebook_link
      t.string :link_instagram
      t.string :link_pinterest
      t.string :institutional_title
      t.string :institutional_text
      t.string :senders_contact
      t.string :senders_where_to_find
      t.string :commercial_catalog_file_name
      t.string :commercial_catalog_content_type
      t.string :commercial_catalog_file_size
      t.string :commercial_catalog_updated_at
      t.string :technical_guide_file_name
      t.string :technical_guide_content_type
      t.string :technical_guide_file_size
      t.string :technical_guide_updated_at
      t.string :institutional_image_file_name
      t.string :institutional_image_content_type
      t.string :institutional_image_file_size
      t.string :institutional_image_updated_at

      t.timestamps null: false
    end
  end
end
