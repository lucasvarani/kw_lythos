class CreateProductLeads < ActiveRecord::Migration
  def change
    create_table :product_leads do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :product

      t.timestamps null: false
    end
  end
end
