class CreateFindIts < ActiveRecord::Migration
  def change
    create_table :find_its do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :city
      t.string :state
      t.boolean :published ,:null => false, :default => 1
      t.boolean :active,:null => false, :default => 1

      t.timestamps null: false
    end
  end
end
