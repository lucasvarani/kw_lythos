class CreateHighlights < ActiveRecord::Migration
  def change
    create_table :highlights do |t|
      t.string :title
      t.string :subtitle
      t.datetime :publication_start
      t.datetime :publication_stop
      t.integer :order,:null => false, :default => 0
      t.boolean :published,:null => false, :default => 1
      t.boolean :active,:null => false, :default => 1
      t.string :image_highlight
      t.string :thumbnail
      t.string :thumbnail_shadow
      t.string :link
      t.string :color1,:null => false, :default => '5c0001'
      t.string :color2,:null => false, :default => '8c0708'
      t.string :color3,:null => false, :default => 'c00708'
      t.string :color4,:null => false, :default => 'f75052'

      t.timestamps null: false
    end
  end
end
