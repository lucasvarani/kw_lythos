class CreateProductHighlights < ActiveRecord::Migration
  def change
    create_table :product_highlights do |t|
      t.integer :product_id
      t.string :title
      t.string :subtitle
      t.datetime :publication_start
      t.datetime :publication_stop
      t.integer :order,:null => false, :default => 0
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.string :link
      t.string :color1
      t.string :color2
      t.string :color3
      t.string :color4
      t.string :highlight_product
      t.string :short_text

      t.timestamps null: false
    end
  end
end
