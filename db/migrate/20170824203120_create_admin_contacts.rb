class CreateAdminContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :message
      t.boolean :published,:null => false, :default => 1
      t.string :state
      t.string :business
      t.string :city
      t.boolean :active,:null => false, :default => 1

      t.timestamps null: false
    end
  end
end
