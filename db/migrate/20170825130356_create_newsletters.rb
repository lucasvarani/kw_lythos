class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.string :email
      t.boolean :active,:null => false, :default => 1

      t.timestamps null: false
    end
  end
end
